package com.example.localadmin.test1.data;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.localadmin.test1.MainActivity;
import com.example.localadmin.test1.R;
import com.example.localadmin.test1.data.model.User;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by localadmin on 12/03/2018.
 */

public class UserAdapter
        extends RecyclerView.Adapter<UserAdapter.UserViewHolder>
        implements View.OnClickListener {
    private ArrayList<User> mUsers;
    private View.OnClickListener mListener;

    public UserAdapter(ArrayList<User> users) {
        this.mUsers = users;
    }

    public UserAdapter() {
        this.mUsers = new ArrayList<>();
    }

    public void setUsers(ArrayList<User> users) {
        this.mUsers = users;
    }

    @Override
    public UserViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_user, parent, false);
        itemView.setOnClickListener(this);
        UserViewHolder userVH = new UserViewHolder(itemView);

        return userVH;
    }

    @Override
    public void onBindViewHolder(UserViewHolder holder, int position) {
        User user = mUsers.get(position);
        holder.bindUser(user);
    }

    @Override
    public int getItemCount() {
        return mUsers.size();
    }

    public Object getItem(int position) {
        return mUsers.get(position);
    }

    public static class UserViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.item_user_tv_username) TextView username;
        @BindView(R.id.item_user_tv_email) TextView email;

        public UserViewHolder(View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);
        }

        public void bindUser(User user) {
            username.setText(user.getUsername());
            email.setText(user.getEmail());
        }
    }

    public void setOnClickListener(View.OnClickListener listener) {
        this.mListener = listener;
    }

    @Override
    public void onClick(View view) {
        if(mListener != null)
            mListener.onClick(view);
    }
}
