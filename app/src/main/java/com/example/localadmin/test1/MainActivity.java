package com.example.localadmin.test1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.example.localadmin.test1.data.UserAdapter;
import com.example.localadmin.test1.data.UserClient;
import com.example.localadmin.test1.data.model.User;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {
    private ArrayList<User> users;
    @BindView(R.id.activity_main_rv_users) RecyclerView recyclerView;
    UserAdapter userAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);
        init();
    }

    private void init() {
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        userAdapter = new UserAdapter();
        userAdapter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                User userSelected = (User) userAdapter.getItem(recyclerView.getChildAdapterPosition(view));
                Intent toUserData = new Intent(MainActivity.this, );
            }
        });
        recyclerView.setAdapter(userAdapter);
        recyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));

        loadJsonOfUsersByRest();
    }

    private void loadJsonOfUsersByRest() {
        Gson gson = new GsonBuilder()
                .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://jsonplaceholder.typicode.com/")
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
        UserClient userClient = retrofit.create(UserClient.class);
        Call<List<User>> userCall = userClient.getData();

        userCall.enqueue(new Callback<List<User>>() {
            @Override
            public void onResponse(Call<List<User>> call, Response<List<User>> response) {
                switch (response.code()) {
                    case 200:
                        users = (ArrayList) response.body();
                        userAdapter.setUsers(users);
                        userAdapter.notifyDataSetChanged();
                        System.out.println(users.toString());
                        break;
                    default:
                        System.out.println("default");
                        break;
                }
            }

            @Override
            public void onFailure(Call<List<User>> call, Throwable t) {
                Log.e("error", t.toString());
            }
        });
    }
}
