package com.example.localadmin.test1.data;

import com.example.localadmin.test1.data.model.User;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by localadmin on 12/03/2018.
 */

public interface UserClient {
    @GET("users")
    Call<List<User>> getData();
}
